Github-Boilerplate
==================

A sort of HTML5 boilerplate, but for Github repositories. Includes a README, CONTRIBUTING.md and all that sort of fancy stuff.
